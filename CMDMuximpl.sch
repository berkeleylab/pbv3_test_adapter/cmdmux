EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "Powerboard v3 Massive Active Tester Board"
Date "2019-06-04"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 2700 4600 0    60   ~ 0
EN0
Text Label 2700 4700 0    60   ~ 0
EN1
Text Label 2700 4800 0    60   ~ 0
EN2
Text Label 2700 4900 0    60   ~ 0
EN3
$Comp
L power:+3V3 #PWR013
U 1 1 5CEC1FB7
P 1875 4275
F 0 "#PWR013" H 1875 4125 50  0001 C CNN
F 1 "+3V3" H 1890 4448 50  0000 C CNN
F 2 "" H 1875 4275 50  0001 C CNN
F 3 "" H 1875 4275 50  0001 C CNN
	1    1875 4275
	1    0    0    -1  
$EndComp
Wire Wire Line
	1875 4325 1875 4275
$Comp
L power:GND #PWR08
U 1 1 5CECA31C
P 1025 5200
F 0 "#PWR08" H 1025 4950 50  0001 C CNN
F 1 "GND" H 975 5050 50  0000 C CNN
F 2 "" H 1025 5200 50  0001 C CNN
F 3 "" H 1025 5200 50  0001 C CNN
	1    1025 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5CECC797
P 800 4850
F 0 "R3" H 870 4896 50  0000 L CNN
F 1 "10k" H 870 4805 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 730 4850 50  0001 C CNN
F 3 "~" H 800 4850 50  0001 C CNN
	1    800  4850
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR07
U 1 1 5CECD8CD
P 800 4650
F 0 "#PWR07" H 800 4500 50  0001 C CNN
F 1 "+3V3" H 815 4823 50  0000 C CNN
F 2 "" H 800 4650 50  0001 C CNN
F 3 "" H 800 4650 50  0001 C CNN
	1    800  4650
	1    0    0    -1  
$EndComp
Text HLabel 1100 4600 0    60   Input ~ 0
SDA
Text HLabel 1100 4700 0    60   Input ~ 0
SCL
$Comp
L Device:C C1
U 1 1 5CED0185
P 1375 4025
F 0 "C1" H 1490 4071 50  0000 L CNN
F 1 "0.1uF" H 1490 3980 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1413 3875 50  0001 C CNN
F 3 "~" H 1375 4025 50  0001 C CNN
	1    1375 4025
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR09
U 1 1 5CED0A0D
P 1375 3825
F 0 "#PWR09" H 1375 3675 50  0001 C CNN
F 1 "+3V3" H 1390 3998 50  0000 C CNN
F 2 "" H 1375 3825 50  0001 C CNN
F 3 "" H 1375 3825 50  0001 C CNN
	1    1375 3825
	1    0    0    -1  
$EndComp
Wire Wire Line
	1375 3825 1375 3875
$Comp
L power:GND #PWR010
U 1 1 5CED1A40
P 1375 4225
F 0 "#PWR010" H 1375 3975 50  0001 C CNN
F 1 "GND" H 1380 4052 50  0000 C CNN
F 2 "" H 1375 4225 50  0001 C CNN
F 3 "" H 1375 4225 50  0001 C CNN
	1    1375 4225
	1    0    0    -1  
$EndComp
Wire Wire Line
	1375 4225 1375 4175
Text HLabel 1100 2800 0    60   Input ~ 12
CMDin_P
Text HLabel 1100 2950 0    60   Input ~ 12
CMDin_N
Wire Wire Line
	1100 2950 1250 2950
Wire Wire Line
	1250 2800 1100 2800
Entry Wire Line
	1050 6600 1150 6700
Entry Wire Line
	1050 6700 1150 6800
Entry Wire Line
	1050 6800 1150 6900
Entry Wire Line
	1050 6900 1150 7000
Entry Wire Line
	1050 7000 1150 7100
Entry Wire Line
	1050 7100 1150 7200
Entry Wire Line
	1050 7200 1150 7300
Entry Wire Line
	1050 7300 1150 7400
Entry Wire Line
	1050 7400 1150 7500
Wire Wire Line
	1050 6600 1000 6600
Wire Wire Line
	1050 6700 1000 6700
Wire Wire Line
	1050 6800 1000 6800
Wire Wire Line
	1050 6900 1000 6900
Wire Wire Line
	1050 7000 1000 7000
Wire Wire Line
	1050 7100 1000 7100
Wire Wire Line
	1050 7200 1000 7200
Wire Wire Line
	1050 7300 1000 7300
Wire Wire Line
	1050 7400 1000 7400
Entry Wire Line
	1050 7500 1150 7600
Wire Wire Line
	1050 7500 1000 7500
Text HLabel 1200 7700 2    60   Output ~ 12
CMDin_P[0..9]
Wire Wire Line
	1900 6600 1850 6600
Wire Wire Line
	1900 6700 1850 6700
Wire Wire Line
	1900 6800 1850 6800
Wire Wire Line
	1900 6900 1850 6900
Wire Wire Line
	1900 7000 1850 7000
Wire Wire Line
	1900 7100 1850 7100
Wire Wire Line
	1900 7200 1850 7200
Wire Wire Line
	1900 7300 1850 7300
Wire Wire Line
	1900 7400 1850 7400
Wire Wire Line
	1900 7500 1850 7500
Text HLabel 2050 7700 2    60   Output ~ 12
CMDin_N[0..9]
Text Label 1000 6600 2    60   ~ 12
CMDin_P0
Text Label 1000 6700 2    60   ~ 12
CMDin_P1
Text Label 1000 6800 2    60   ~ 12
CMDin_P2
Text Label 1000 6900 2    60   ~ 12
CMDin_P3
Text Label 1000 7000 2    60   ~ 12
CMDin_P4
Text Label 1000 7100 2    60   ~ 12
CMDin_P5
Text Label 1000 7200 2    60   ~ 12
CMDin_P6
Text Label 1000 7300 2    60   ~ 12
CMDin_P7
Text Label 1000 7400 2    60   ~ 12
CMDin_P8
Text Label 1000 7500 2    60   ~ 12
CMDin_P9
Text Label 1850 6600 2    60   ~ 0
CMDin_N0
Text Label 1850 6700 2    60   ~ 0
CMDin_N1
Text Label 1850 6800 2    60   ~ 12
CMDin_N2
Text Label 1850 6900 2    60   ~ 12
CMDin_N3
Text Label 1850 7000 2    60   ~ 12
CMDin_N4
Text Label 1850 7100 2    60   ~ 12
CMDin_N5
Text Label 1850 7200 2    60   ~ 12
CMDin_N6
Text Label 1850 7300 2    60   ~ 12
CMDin_N7
Text Label 1850 7400 2    60   ~ 12
CMDin_N8
Text Label 1850 7500 2    60   ~ 12
CMDin_N9
Wire Bus Line
	1200 7700 1150 7700
Entry Wire Line
	1900 6600 2000 6700
Entry Wire Line
	1900 6700 2000 6800
Entry Wire Line
	1900 6800 2000 6900
Entry Wire Line
	1900 6900 2000 7000
Entry Wire Line
	1900 7000 2000 7100
Entry Wire Line
	1900 7100 2000 7200
Entry Wire Line
	1900 7200 2000 7300
Entry Wire Line
	1900 7300 2000 7400
Entry Wire Line
	1900 7400 2000 7500
Entry Wire Line
	1900 7500 2000 7600
Wire Bus Line
	2050 7700 2000 7700
$Comp
L lbl_lvds:PI3PCIE3212 U1
U 1 1 5CF7AAC0
P 1350 1850
F 0 "U1" H 1150 1675 60  0000 R CNN
F 1 "PI3PCIE3212" H 1150 1575 60  0000 R CNN
F 2 "lbl_packages:TQFN-20" H 1350 1850 60  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/PI3PCIE3212.pdf" H 1350 1850 60  0001 C CNN
F 4 "PI3PCIE3212ZBEX" H 1350 1850 50  0001 C CNN "mfg#"
	1    1350 1850
	1    0    0    -1  
$EndComp
Text Label 900  1100 2    60   ~ 0
CMDin_P
Text Label 900  1200 2    60   ~ 0
CMDin_N
Wire Wire Line
	1250 800  1350 800 
Connection ~ 1350 800 
Wire Wire Line
	1350 800  1450 800 
Wire Wire Line
	1350 800  1350 750 
$Comp
L power:+3V3 #PWR011
U 1 1 5CF8367E
P 1350 750
F 0 "#PWR011" H 1350 600 50  0001 C CNN
F 1 "+3V3" H 1365 923 50  0000 C CNN
F 2 "" H 1350 750 50  0001 C CNN
F 3 "" H 1350 750 50  0001 C CNN
	1    1350 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 2050 1350 2050
Connection ~ 1350 2050
$Comp
L power:GND #PWR012
U 1 1 5CF853A8
P 1350 2100
F 0 "#PWR012" H 1350 1850 50  0001 C CNN
F 1 "GND" H 1355 1927 50  0000 C CNN
F 2 "" H 1350 2100 50  0001 C CNN
F 3 "" H 1350 2100 50  0001 C CNN
	1    1350 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 2100 1350 2050
Wire Wire Line
	3250 800  3350 800 
Connection ~ 3350 800 
Wire Wire Line
	3350 800  3450 800 
Wire Wire Line
	3350 800  3350 750 
$Comp
L power:+3V3 #PWR015
U 1 1 5CF878DA
P 3350 750
F 0 "#PWR015" H 3350 600 50  0001 C CNN
F 1 "+3V3" H 3365 923 50  0000 C CNN
F 2 "" H 3350 750 50  0001 C CNN
F 3 "" H 3350 750 50  0001 C CNN
	1    3350 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2050 3350 2050
Connection ~ 3350 2050
Wire Wire Line
	3350 2050 3450 2050
$Comp
L power:GND #PWR016
U 1 1 5CF878E7
P 3350 2100
F 0 "#PWR016" H 3350 1850 50  0001 C CNN
F 1 "GND" H 3355 1927 50  0000 C CNN
F 2 "" H 3350 2100 50  0001 C CNN
F 3 "" H 3350 2100 50  0001 C CNN
	1    3350 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 2100 3350 2050
Text Label 900  1800 2    60   ~ 0
MUX_EN
Text Label 2900 1800 2    60   ~ 12
MUX_EN
$Comp
L lbl_lvds:PI3PCIE3212 U4
U 1 1 5CF9A673
P 3350 3650
F 0 "U4" H 3150 3475 60  0000 R CNN
F 1 "PI3PCIE3212" H 3150 3375 60  0000 R CNN
F 2 "lbl_packages:TQFN-20" H 3350 3650 60  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/PI3PCIE3212.pdf" H 3350 3650 60  0001 C CNN
	1    3350 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2600 3350 2600
Connection ~ 3350 2600
Wire Wire Line
	3350 2600 3450 2600
Wire Wire Line
	3350 2600 3350 2550
$Comp
L power:+3V3 #PWR017
U 1 1 5CF9A681
P 3350 2550
F 0 "#PWR017" H 3350 2400 50  0001 C CNN
F 1 "+3V3" H 3365 2723 50  0000 C CNN
F 2 "" H 3350 2550 50  0001 C CNN
F 3 "" H 3350 2550 50  0001 C CNN
	1    3350 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3850 3350 3850
Connection ~ 3350 3850
Wire Wire Line
	3350 3850 3450 3850
$Comp
L power:GND #PWR018
U 1 1 5CF9A68E
P 3350 3900
F 0 "#PWR018" H 3350 3650 50  0001 C CNN
F 1 "GND" H 3355 3727 50  0000 C CNN
F 2 "" H 3350 3900 50  0001 C CNN
F 3 "" H 3350 3900 50  0001 C CNN
	1    3350 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 3900 3350 3850
Text Label 2900 3600 2    60   ~ 12
MUX_EN
Text Label 1800 1000 0    60   ~ 12
CMDin_0_P
Text Label 1800 1100 0    60   ~ 12
CMDin_0_N
Text Label 1800 1200 0    60   ~ 12
CMDin_1_P
Text Label 1800 1300 0    60   ~ 12
CMDin_1_N
Text Label 2900 1100 2    60   ~ 12
CMDin_0_P
Text Label 2900 1200 2    60   ~ 12
CMDin_0_N
Text Label 2900 2900 2    60   ~ 12
CMDin_1_P
Text Label 2900 3000 2    60   ~ 12
CMDin_1_N
Text Label 3800 1000 0    60   ~ 12
CMDin_0_0_P
Text Label 3800 1100 0    60   ~ 12
CMDin_0_0_N
Text Label 3800 1200 0    60   ~ 12
CMDin_0_1_P
Text Label 3800 1300 0    60   ~ 12
CMDin_0_1_N
Text Label 3800 2800 0    60   ~ 12
CMDin_1_0_P
Text Label 3800 2900 0    60   ~ 12
CMDin_1_0_N
Text Label 3800 3000 0    60   ~ 12
CMDin_1_1_P
Text Label 3800 3100 0    60   ~ 12
CMDin_1_1_N
Wire Wire Line
	5450 800  5550 800 
Connection ~ 5550 800 
Wire Wire Line
	5550 800  5650 800 
Wire Wire Line
	5550 800  5550 750 
$Comp
L power:+3V3 #PWR019
U 1 1 5CFBC328
P 5550 750
F 0 "#PWR019" H 5550 600 50  0001 C CNN
F 1 "+3V3" H 5565 923 50  0000 C CNN
F 2 "" H 5550 750 50  0001 C CNN
F 3 "" H 5550 750 50  0001 C CNN
	1    5550 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2050 5550 2050
Connection ~ 5550 2050
Wire Wire Line
	5550 2050 5650 2050
$Comp
L power:GND #PWR020
U 1 1 5CFBC335
P 5550 2100
F 0 "#PWR020" H 5550 1850 50  0001 C CNN
F 1 "GND" H 5555 1927 50  0000 C CNN
F 2 "" H 5550 2100 50  0001 C CNN
F 3 "" H 5550 2100 50  0001 C CNN
	1    5550 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2100 5550 2050
Text Label 5100 1800 2    60   ~ 12
MUX_EN
$Comp
L lbl_lvds:PI3PCIE3212 U6
U 1 1 5CFBC341
P 5550 3650
F 0 "U6" H 5350 3475 60  0000 R CNN
F 1 "PI3PCIE3212" H 5350 3375 60  0000 R CNN
F 2 "lbl_packages:TQFN-20" H 5550 3650 60  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/PI3PCIE3212.pdf" H 5550 3650 60  0001 C CNN
	1    5550 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2600 5550 2600
Connection ~ 5550 2600
Wire Wire Line
	5550 2600 5650 2600
Wire Wire Line
	5550 2600 5550 2550
$Comp
L power:+3V3 #PWR021
U 1 1 5CFBC34F
P 5550 2550
F 0 "#PWR021" H 5550 2400 50  0001 C CNN
F 1 "+3V3" H 5565 2723 50  0000 C CNN
F 2 "" H 5550 2550 50  0001 C CNN
F 3 "" H 5550 2550 50  0001 C CNN
	1    5550 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3850 5550 3850
Connection ~ 5550 3850
Wire Wire Line
	5550 3850 5650 3850
$Comp
L power:GND #PWR022
U 1 1 5CFBC35C
P 5550 3900
F 0 "#PWR022" H 5550 3650 50  0001 C CNN
F 1 "GND" H 5555 3727 50  0000 C CNN
F 2 "" H 5550 3900 50  0001 C CNN
F 3 "" H 5550 3900 50  0001 C CNN
	1    5550 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3900 5550 3850
Text Label 5100 3600 2    60   ~ 12
MUX_EN
Text Label 5100 1100 2    60   ~ 12
CMDin_0_0_P
Text Label 5100 1200 2    60   ~ 12
CMDin_0_0_N
Text Label 5100 2900 2    60   ~ 12
CMDin_0_1_P
Text Label 5100 3000 2    60   ~ 12
CMDin_0_1_N
Text Label 6000 1000 0    60   ~ 12
CMDin_0_0_0_P
Text Label 6000 1100 0    60   ~ 12
CMDin_0_0_0_N
Text Label 6000 1200 0    60   ~ 12
CMDin_0_0_1_P
Text Label 6000 1300 0    60   ~ 12
CMDin_0_0_1_N
$Comp
L lbl_lvds:PI3PCIE3212 U7
U 1 1 5CFC8324
P 5550 5450
F 0 "U7" H 5350 5275 60  0000 R CNN
F 1 "PI3PCIE3212" H 5350 5175 60  0000 R CNN
F 2 "lbl_packages:TQFN-20" H 5550 5450 60  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/PI3PCIE3212.pdf" H 5550 5450 60  0001 C CNN
	1    5550 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 4400 5550 4400
Connection ~ 5550 4400
Wire Wire Line
	5550 4400 5650 4400
Wire Wire Line
	5550 4400 5550 4350
$Comp
L power:+3V3 #PWR023
U 1 1 5CFC8332
P 5550 4350
F 0 "#PWR023" H 5550 4200 50  0001 C CNN
F 1 "+3V3" H 5565 4523 50  0000 C CNN
F 2 "" H 5550 4350 50  0001 C CNN
F 3 "" H 5550 4350 50  0001 C CNN
	1    5550 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 5650 5550 5650
Connection ~ 5550 5650
Wire Wire Line
	5550 5650 5650 5650
$Comp
L power:GND #PWR024
U 1 1 5CFC833F
P 5550 5700
F 0 "#PWR024" H 5550 5450 50  0001 C CNN
F 1 "GND" H 5555 5527 50  0000 C CNN
F 2 "" H 5550 5700 50  0001 C CNN
F 3 "" H 5550 5700 50  0001 C CNN
	1    5550 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 5700 5550 5650
Text Label 5100 5400 2    60   ~ 12
MUX_EN
$Comp
L lbl_lvds:PI3PCIE3212 U8
U 1 1 5CFC834B
P 5550 7250
F 0 "U8" H 5350 7075 60  0000 R CNN
F 1 "PI3PCIE3212" H 5350 6975 60  0000 R CNN
F 2 "lbl_packages:TQFN-20" H 5550 7250 60  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/PI3PCIE3212.pdf" H 5550 7250 60  0001 C CNN
	1    5550 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 6200 5550 6200
Connection ~ 5550 6200
Wire Wire Line
	5550 6200 5650 6200
Wire Wire Line
	5550 6200 5550 6150
$Comp
L power:+3V3 #PWR025
U 1 1 5CFC8359
P 5550 6150
F 0 "#PWR025" H 5550 6000 50  0001 C CNN
F 1 "+3V3" H 5565 6323 50  0000 C CNN
F 2 "" H 5550 6150 50  0001 C CNN
F 3 "" H 5550 6150 50  0001 C CNN
	1    5550 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 7450 5550 7450
Connection ~ 5550 7450
Wire Wire Line
	5550 7450 5650 7450
$Comp
L power:GND #PWR026
U 1 1 5CFC8366
P 5550 7500
F 0 "#PWR026" H 5550 7250 50  0001 C CNN
F 1 "GND" H 5555 7327 50  0000 C CNN
F 2 "" H 5550 7500 50  0001 C CNN
F 3 "" H 5550 7500 50  0001 C CNN
	1    5550 7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 7500 5550 7450
Text Label 5100 7200 2    60   ~ 12
MUX_EN
Text Label 5100 4700 2    60   ~ 12
CMDin_1_0_P
Text Label 5100 4800 2    60   ~ 12
CMDin_1_0_N
Text Label 5100 6500 2    60   ~ 12
CMDin_1_1_P
Text Label 5100 6600 2    60   ~ 12
CMDin_1_1_N
Wire Wire Line
	8450 800  8550 800 
Connection ~ 8550 800 
Wire Wire Line
	8550 800  8650 800 
Wire Wire Line
	8550 800  8550 750 
$Comp
L power:+3V3 #PWR027
U 1 1 5D0196A3
P 8550 750
F 0 "#PWR027" H 8550 600 50  0001 C CNN
F 1 "+3V3" H 8565 923 50  0000 C CNN
F 2 "" H 8550 750 50  0001 C CNN
F 3 "" H 8550 750 50  0001 C CNN
	1    8550 750 
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8450 2050 8550 2050
Connection ~ 8550 2050
Wire Wire Line
	8550 2050 8650 2050
$Comp
L power:GND #PWR028
U 1 1 5D0196B0
P 8550 2100
F 0 "#PWR028" H 8550 1850 50  0001 C CNN
F 1 "GND" H 8555 1927 50  0000 C CNN
F 2 "" H 8550 2100 50  0001 C CNN
F 3 "" H 8550 2100 50  0001 C CNN
	1    8550 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 2100 8550 2050
Text Label 8100 1800 2    60   ~ 12
MUX_EN
$Comp
L lbl_lvds:PI3PCIE3212 U10
U 1 1 5D0196BC
P 8550 3650
F 0 "U10" H 8350 3475 60  0000 R CNN
F 1 "PI3PCIE3212" H 8350 3375 60  0000 R CNN
F 2 "lbl_packages:TQFN-20" H 8550 3650 60  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/PI3PCIE3212.pdf" H 8550 3650 60  0001 C CNN
	1    8550 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 2600 8550 2600
Connection ~ 8550 2600
Wire Wire Line
	8550 2600 8650 2600
Wire Wire Line
	8550 2600 8550 2550
$Comp
L power:+3V3 #PWR029
U 1 1 5D0196CA
P 8550 2550
F 0 "#PWR029" H 8550 2400 50  0001 C CNN
F 1 "+3V3" H 8565 2723 50  0000 C CNN
F 2 "" H 8550 2550 50  0001 C CNN
F 3 "" H 8550 2550 50  0001 C CNN
	1    8550 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 3850 8550 3850
Connection ~ 8550 3850
Wire Wire Line
	8550 3850 8650 3850
$Comp
L power:GND #PWR030
U 1 1 5D0196D7
P 8550 3900
F 0 "#PWR030" H 8550 3650 50  0001 C CNN
F 1 "GND" H 8555 3727 50  0000 C CNN
F 2 "" H 8550 3900 50  0001 C CNN
F 3 "" H 8550 3900 50  0001 C CNN
	1    8550 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 3900 8550 3850
Text Label 8100 3600 2    60   ~ 12
MUX_EN
Text Label 8100 1100 2    60   ~ 12
CMDin_0_0_0_P
Text Label 8100 1200 2    60   ~ 12
CMDin_0_0_0_N
Text Label 8100 2900 2    60   ~ 12
CMDin_0_0_1_P
Text Label 8100 3000 2    60   ~ 12
CMDin_0_0_1_N
Wire Wire Line
	9000 1000 9850 1000
Text Label 6850 3300 0    60   ~ 0
CMDin_P0
Text Label 6850 3200 0    60   ~ 0
CMDin_N0
Wire Wire Line
	9000 1100 9850 1100
Wire Wire Line
	9000 1200 9850 1200
Wire Wire Line
	9000 1300 9850 1300
Wire Wire Line
	9000 2800 9850 2800
Wire Wire Line
	9000 2900 9850 2900
Wire Wire Line
	9000 3000 9850 3000
Wire Wire Line
	9000 3100 9850 3100
Text Label 9850 3500 0    60   ~ 0
CMDin_P1
Text Label 9850 3400 0    60   ~ 0
CMDin_N1
Text Label 6850 3500 0    60   ~ 0
CMDin_P9
Text Label 6850 3400 0    60   ~ 0
CMDin_N9
Wire Wire Line
	6000 4600 6850 4600
Wire Wire Line
	6000 4700 6850 4700
Wire Wire Line
	6000 4800 6850 4800
Wire Wire Line
	6000 4900 6850 4900
Text Label 9850 1500 0    60   ~ 0
CMDin_P4
Text Label 9850 1400 0    60   ~ 0
CMDin_N4
Text Label 6850 7000 0    60   ~ 0
CMDin_P5
Text Label 6850 7100 0    60   ~ 0
CMDin_N5
Wire Wire Line
	6000 2800 6850 2800
Wire Wire Line
	6000 2900 6850 2900
Wire Wire Line
	6000 3000 6850 3000
Wire Wire Line
	6000 3100 6850 3100
Text Label 9850 3300 0    60   ~ 0
CMDin_P2
Text Label 9850 3200 0    60   ~ 0
CMDin_N2
Text Label 9850 1700 0    60   ~ 0
CMDin_P3
Text Label 9850 1600 0    60   ~ 0
CMDin_N3
Text Label 900  1500 2    60   ~ 0
CMDout_P
Text Label 900  1600 2    60   ~ 0
CMDout_N
Text Label 1800 1500 0    60   ~ 12
CMDout_0_P
Text Label 1800 1400 0    60   ~ 12
CMDout_0_N
Text Label 1800 1700 0    60   ~ 12
CMDout_1_P
Text Label 1800 1600 0    60   ~ 12
CMDout_1_N
Text Label 2900 1600 2    60   ~ 12
CMDout_0_P
Text Label 2900 1500 2    60   ~ 12
CMDout_0_N
Text Label 3800 1500 0    60   ~ 12
CMDout_0_0_P
Text Label 3800 1400 0    60   ~ 12
CMDout_0_0_N
Text Label 3800 1700 0    60   ~ 12
CMDout_0_1_P
Text Label 3800 1600 0    60   ~ 12
CMDout_0_1_N
Text Label 2900 3400 2    60   ~ 12
CMDout_1_P
Text Label 2900 3300 2    60   ~ 12
CMDout_1_N
Text Label 3800 3300 0    60   ~ 12
CMDout_1_0_P
Text Label 3800 3200 0    60   ~ 12
CMDout_1_0_N
Text Label 3800 3500 0    60   ~ 12
CMDout_1_1_P
Text Label 3800 3400 0    60   ~ 12
CMDout_1_1_N
Text Label 5100 1600 2    60   ~ 12
CMDout_0_0_P
Text Label 5100 1500 2    60   ~ 12
CMDout_0_0_N
Text Label 5100 3400 2    60   ~ 12
CMDout_0_1_P
Text Label 5100 3300 2    60   ~ 12
CMDout_0_1_N
Text Label 5100 5200 2    60   ~ 12
CMDout_1_0_P
Text Label 5100 5100 2    60   ~ 12
CMDout_1_0_N
Text Label 5100 7000 2    60   ~ 12
CMDout_1_1_P
Text Label 5100 6900 2    60   ~ 12
CMDout_1_1_N
Text Label 6000 1500 0    60   ~ 12
CMDout_0_0_0_P
Text Label 6000 1400 0    60   ~ 12
CMDout_0_0_0_N
Text Label 6000 1700 0    60   ~ 12
CMDout_0_0_1_P
Text Label 6000 1600 0    60   ~ 12
CMDout_0_0_1_N
Wire Wire Line
	6000 3200 6850 3200
Wire Wire Line
	6000 3300 6850 3300
Wire Wire Line
	6000 3400 6850 3400
Wire Wire Line
	6000 3500 6850 3500
Text Label 9850 2900 0    60   ~ 0
CMDout_P2
Wire Wire Line
	6000 5100 6850 5100
Wire Wire Line
	6000 5000 6850 5000
Wire Wire Line
	6000 5300 6850 5300
Text Label 9850 1100 0    60   ~ 0
CMDout_P4
Text Label 9850 1000 0    60   ~ 0
CMDout_N4
Text Label 6850 6600 0    60   ~ 0
CMDout_P5
Text Label 6850 6700 0    60   ~ 0
CMDout_N5
Text Label 6850 2800 0    60   ~ 0
CMDout_N0
Wire Wire Line
	9000 3200 9850 3200
Wire Wire Line
	9000 3300 9850 3300
Wire Wire Line
	9000 3400 9850 3400
Wire Wire Line
	9000 3500 9850 3500
Text Label 9850 3100 0    60   ~ 0
CMDout_P1
Text Label 9850 3000 0    60   ~ 0
CMDout_N1
Text Label 6850 3000 0    60   ~ 0
CMDout_P9
Text Label 6850 3100 0    60   ~ 0
CMDout_N9
Text Label 8100 1600 2    60   ~ 12
CMDout_0_0_0_P
Text Label 8100 1500 2    60   ~ 12
CMDout_0_0_0_N
Text Label 8100 3400 2    60   ~ 0
CMDout_0_0_1_P
Text Label 8100 3300 2    60   ~ 0
CMDout_0_0_1_N
Text Label 2700 5000 0    60   ~ 0
MUX_EN
Entry Wire Line
	3100 6600 3200 6700
Entry Wire Line
	3100 6700 3200 6800
Entry Wire Line
	3100 6800 3200 6900
Entry Wire Line
	3100 6900 3200 7000
Entry Wire Line
	3100 7000 3200 7100
Entry Wire Line
	3100 7100 3200 7200
Entry Wire Line
	3100 7200 3200 7300
Entry Wire Line
	3100 7300 3200 7400
Entry Wire Line
	3100 7400 3200 7500
Wire Wire Line
	3100 6600 3050 6600
Wire Wire Line
	3100 6700 3050 6700
Wire Wire Line
	3100 6800 3050 6800
Wire Wire Line
	3100 6900 3050 6900
Wire Wire Line
	3100 7000 3050 7000
Wire Wire Line
	3100 7100 3050 7100
Wire Wire Line
	3100 7200 3050 7200
Wire Wire Line
	3100 7300 3050 7300
Wire Wire Line
	3100 7400 3050 7400
Entry Wire Line
	3100 7500 3200 7600
Wire Wire Line
	3100 7500 3050 7500
Wire Wire Line
	3950 6600 3900 6600
Wire Wire Line
	3950 6700 3900 6700
Wire Wire Line
	3950 6800 3900 6800
Wire Wire Line
	3950 6900 3900 6900
Wire Wire Line
	3950 7000 3900 7000
Wire Wire Line
	3950 7100 3900 7100
Wire Wire Line
	3950 7200 3900 7200
Wire Wire Line
	3950 7300 3900 7300
Wire Wire Line
	3950 7400 3900 7400
Wire Wire Line
	3950 7500 3900 7500
Text HLabel 4100 7700 2    60   Input ~ 0
CMDout_N[0..9]
Wire Bus Line
	3250 7700 3200 7700
Entry Wire Line
	3950 6600 4050 6700
Entry Wire Line
	3950 6700 4050 6800
Entry Wire Line
	3950 6800 4050 6900
Entry Wire Line
	3950 6900 4050 7000
Entry Wire Line
	3950 7000 4050 7100
Entry Wire Line
	3950 7100 4050 7200
Entry Wire Line
	3950 7200 4050 7300
Entry Wire Line
	3950 7300 4050 7400
Entry Wire Line
	3950 7400 4050 7500
Entry Wire Line
	3950 7500 4050 7600
Wire Bus Line
	4100 7700 4050 7700
Text Label 3050 6600 2    60   ~ 12
CMDout_P0
Text Label 3050 6700 2    60   ~ 12
CMDout_P1
Text Label 3050 6800 2    60   ~ 12
CMDout_P2
Text Label 3050 6900 2    60   ~ 12
CMDout_P3
Text Label 3050 7000 2    60   ~ 12
CMDout_P4
Text Label 3050 7100 2    60   ~ 12
CMDout_P5
Text Label 3050 7200 2    60   ~ 12
CMDout_P6
Text Label 3050 7300 2    60   ~ 12
CMDout_P7
Text Label 3050 7400 2    60   ~ 0
CMDout_P8
Text Label 3050 7500 2    60   ~ 0
CMDout_P9
Text Label 3900 6600 2    60   ~ 12
CMDout_N0
Text Label 3900 6700 2    60   ~ 12
CMDout_N1
Text Label 3900 6800 2    60   ~ 12
CMDout_N2
Text Label 3900 6900 2    60   ~ 12
CMDout_N3
Text Label 3900 7000 2    60   ~ 12
CMDout_N4
Text Label 3900 7100 2    60   ~ 12
CMDout_N5
Text Label 3900 7200 2    60   ~ 12
CMDout_N6
Text Label 3900 7300 2    60   ~ 12
CMDout_N7
Text Label 3900 7400 2    60   ~ 0
CMDout_N8
Text Label 3900 7500 2    60   ~ 0
CMDout_N9
Text HLabel 1200 3100 2    60   Output ~ 12
CMDout_P
Text HLabel 1200 3250 2    60   Output ~ 12
CMDout_N
Wire Wire Line
	1200 3100 1050 3100
Wire Wire Line
	1050 3250 1200 3250
Text Label 5100 1900 2    60   ~ 12
EN0
Text Label 5100 3700 2    60   ~ 12
EN0
Text Label 5100 5500 2    60   ~ 12
EN0
Text Label 5100 7300 2    60   ~ 12
EN0
Text Label 2900 3700 2    60   ~ 12
EN1
Text Label 2900 1900 2    60   ~ 12
EN1
Text Label 900  1900 2    60   ~ 0
EN2
Text Label 8100 3700 2    60   ~ 12
EN3
Text Label 8100 1900 2    60   ~ 12
EN3
Wire Wire Line
	6000 6400 6850 6400
Wire Wire Line
	6000 6500 6850 6500
Wire Wire Line
	6000 6600 6850 6600
Wire Wire Line
	6000 6700 6850 6700
Text Label 6850 5300 0    60   ~ 0
CMDin_P7
Text Label 6850 5200 0    60   ~ 0
CMDin_N7
Wire Wire Line
	6000 6800 6850 6800
Wire Wire Line
	6000 6900 6850 6900
Wire Wire Line
	6000 7000 6850 7000
Wire Wire Line
	6000 7100 6850 7100
Text Label 6850 4900 0    60   ~ 0
CMDout_P7
Wire Wire Line
	9000 1500 9850 1500
Wire Wire Line
	9850 1600 9000 1600
Wire Wire Line
	9000 1700 9850 1700
Wire Wire Line
	6000 5200 6850 5200
Text Label 6850 2900 0    60   ~ 0
CMDout_P0
Text HLabel 3250 7700 2    60   Input ~ 0
CMDout_P[0..9]
Text Label 9850 1300 0    60   ~ 0
CMDout_P3
Text Label 9850 1200 0    60   ~ 0
CMDout_N3
Text Label 9850 2800 0    60   ~ 0
CMDout_N2
$Comp
L lbl_lvds:PI3PCIE3212 U9
U 1 1 5D019695
P 8550 1850
F 0 "U9" H 8350 1675 60  0000 R CNN
F 1 "PI3PCIE3212" H 8350 1575 60  0000 R CNN
F 2 "lbl_packages:TQFN-20" H 8550 1850 60  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/PI3PCIE3212.pdf" H 8550 1850 60  0001 C CNN
	1    8550 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 2050 8850 2050
Connection ~ 8650 2050
$Comp
L lbl_lvds:PI3PCIE3212 U5
U 1 1 5CFBC31A
P 5550 1850
F 0 "U5" H 5350 1675 60  0000 R CNN
F 1 "PI3PCIE3212" H 5350 1575 60  0000 R CNN
F 2 "lbl_packages:TQFN-20" H 5550 1850 60  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/PI3PCIE3212.pdf" H 5550 1850 60  0001 C CNN
	1    5550 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 2050 5850 2050
Connection ~ 5650 2050
$Comp
L lbl_lvds:PI3PCIE3212 U3
U 1 1 5CF878CA
P 3350 1850
F 0 "U3" H 3150 1675 60  0000 R CNN
F 1 "PI3PCIE3212" H 3150 1575 60  0000 R CNN
F 2 "lbl_packages:TQFN-20" H 3350 1850 60  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/PI3PCIE3212.pdf" H 3350 1850 60  0001 C CNN
	1    3350 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2050 3450 2050
Connection ~ 3450 2050
Wire Wire Line
	1350 2050 1450 2050
Connection ~ 1450 2050
Wire Wire Line
	1450 2050 1650 2050
Wire Wire Line
	8650 3850 8850 3850
Connection ~ 8650 3850
Wire Wire Line
	5650 7450 5850 7450
Connection ~ 5650 7450
Wire Wire Line
	5850 5650 5650 5650
Connection ~ 5650 5650
Wire Wire Line
	3450 3850 3650 3850
Connection ~ 3450 3850
Wire Wire Line
	5650 3850 5850 3850
Connection ~ 5650 3850
Wire Wire Line
	9850 1400 9000 1400
Text Label 6850 4700 0    60   ~ 0
CMDout_P8
Text Label 6850 4600 0    60   ~ 0
CMDout_N8
Text Label 6850 5000 0    60   ~ 0
CMDin_N8
Text Label 6850 5100 0    60   ~ 0
CMDin_P8
Text Label 6850 6800 0    60   ~ 0
CMDin_P6
Text Label 6850 6900 0    60   ~ 0
CMDin_N6
Text Label 6850 6400 0    60   ~ 0
CMDout_P6
Text Label 6850 6500 0    60   ~ 0
CMDout_N6
Text Label 6850 4800 0    60   ~ 0
CMDout_N7
$Comp
L Device:R R10
U 1 1 5FC2E511
P 625 5250
F 0 "R10" H 555 5204 50  0000 R CNN
F 1 "10k" H 555 5295 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 555 5250 50  0001 C CNN
F 3 "~" H 625 5250 50  0001 C CNN
	1    625  5250
	-1   0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR0105
U 1 1 5FC73CCA
P 625 5000
F 0 "#PWR0105" H 625 4850 50  0001 C CNN
F 1 "+3V3" H 640 5173 50  0000 C CNN
F 2 "" H 625 5000 50  0001 C CNN
F 3 "" H 625 5000 50  0001 C CNN
	1    625  5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	625  5400 1200 5400
Wire Wire Line
	625  5000 625  5100
Wire Wire Line
	1200 5200 1025 5200
Wire Wire Line
	1200 5200 1200 5300
Text Label 2700 5100 0    60   ~ 0
unused5
Text Label 2700 5200 0    60   ~ 0
unused6
Text Label 2700 5300 0    60   ~ 0
unused7
$Comp
L power:GND #PWR014
U 1 1 5CEC4DB3
P 1900 5800
F 0 "#PWR014" H 1900 5550 50  0001 C CNN
F 1 "GND" H 1905 5627 50  0000 C CNN
F 2 "" H 1900 5800 50  0001 C CNN
F 3 "" H 1900 5800 50  0001 C CNN
	1    1900 5800
	1    0    0    -1  
$EndComp
$Comp
L lbl_io:MCP23008 U2
U 1 1 5D204B06
P 1900 5400
F 0 "U2" H 2100 5200 50  0000 C CNN
F 1 "MCP23008" H 2250 5100 50  0000 C CNN
F 2 "Package_SO:SOIC-18W_7.5x11.6mm_P1.27mm" H 2000 5200 50  0001 L CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/MCP23008-MCP23S08-Data-Sheet-20001919F.pdf" H 2100 4300 50  0001 L CNN
F 4 "MCP23008T-E/SO" H 1900 5400 50  0001 C CNN "mfg#"
	1    1900 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 4600 2700 4600
Wire Wire Line
	2600 4700 2700 4700
Wire Wire Line
	2600 4800 2700 4800
Wire Wire Line
	2600 4900 2700 4900
Wire Wire Line
	2600 5000 2700 5000
Wire Wire Line
	2600 5100 2700 5100
Wire Wire Line
	2600 5200 2700 5200
Wire Wire Line
	2600 5300 2700 5300
Wire Wire Line
	1900 5700 1900 5800
Wire Wire Line
	1200 4600 1100 4600
Wire Wire Line
	1100 4700 1200 4700
NoConn ~ 1200 4900
Wire Wire Line
	800  4650 800  4700
Connection ~ 1200 5200
Wire Wire Line
	1200 5000 800  5000
Wire Bus Line
	4050 6700 4050 7700
Wire Bus Line
	3200 6700 3200 7700
Wire Bus Line
	2000 6700 2000 7700
Wire Bus Line
	1150 6700 1150 7700
$EndSCHEMATC
